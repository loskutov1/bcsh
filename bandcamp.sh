#!/bin/bash
#        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

# Requires metaflac (flac), curl, and sox (for spectrals), unzip
set -eo pipefail
IFS=$'\n\t'
VERSION="1.5.0"

# -- USER Configuration -- #

# URL to Webserver for Spectral Output
WEB_URL="https://changeme.example.com/specs/"

# Actual location on server for webserver i.e. ~/public_html/
WEB_DIRECTORY="/path/to/html/specs/"

# Announce URL for torrent generation. use "0" for no torrent generation
# Requires mktorrent
TORRENT_ANNOUNCE_URL="0"

# Source Flag (For mktorrent) requires v1.1 or higher., set to "0" to not set source flag
# Using w/ better.py requires modification of mktorrent command syntax in better.py
TORRENT_SOURCE_FLAG="0"

# Add year to foldername
# Options:
#    "NONE" = Don't add Year
#    "AUTOMATIC" = Add based on tag of first flac.
#    2016 =  Manually set to 2016

METADATA_YEAR="AUTOMATIC"

# -- END Configuration -- #

# Load variables from ~/.config/bcsh/config
# Copy USER Configuration block to be persistent between upgrades
if [ -f "${HOME}/.config/bcsh/config" ]; then
    source "${HOME}/.config/bcsh/config"
fi

# Generate UUID to avoid naming conflicts & for spectrals folder
if [ -f /proc/sys/kernel/random/uuid ]
then
    uuid=$(< /proc/sys/kernel/random/uuid)
else
    uuid=$(uuidgen | tr "[A-Z]" "[a-z]")
fi

function main () {
    # -- Important, don't comment out this -- #
    downloadStuff
    # -- Extra analysis / Processing (Recommended) -- #
    # Use # to comment out functions you don't want to use
    addYearToFolder    # Adds year to foldername (I guess somebody wanted it)
    renameTracks       # Bases filenames on tags (Track# - Title) (requires metaflac)
    renameExtraFiles   # Hopefully remove Artist - Album prefix from cover.jpg etc
    checkAlbumTag      # Check for empty album tag
    checkQuality       # Check Bitdepths (Requires metaflac)
    checkEmbededArt    # Check for too big embedded art (extract to file if too large)
    checkPathLengths   # Check/Warn if total path >180 characters
    generateSpec       # Make spectrals (requires sox)
    makeTorrent        # For flac only, better.py makes its own for transcodes.
    #transcode         # Transcode to V0,320 (requires better.py in PATH)
    checkForExtraFiles # Check for non image/flac files
    runSummary         # Restate bitdepth/spectrals link in case you missed them
}

# Capitalization rational
# -----------------------
# camelCase: Functions
# UPPER_CASE: Variable that is reasonably retrieved from another function, hopefully unique in purpose, though it can be overridden later in the script
# lower_case: Variable that is only relevant during the function or loop it is used in

function showHelp () {
    printf "bandcamp.sh ${VERSION}\n"
    printf 'Usage:\t./bandcamp.sh [options] "bandcamp.link.etc"\n'
    printf 'options:\n'
    printf '\t-a|--announce="http://someannounce.url"\n'
    printf '\t-s|--source="SRC"\n'
    printf '\t-y|--year=2016\n'
    printf 'extras:\n'
    printf '\t--tracklist         | Output bbcode formatted tracklist\n'
    printf '\t--gen-spec          | Run Spectral Generator\n'
    printf '\t--check-quality     | Check quality/sample rates\n'
    printf '\t--check-path-length | Check files for too long paths >180 characters total\n'
    return 1
}

function downloadStuff () {
    #TODO Find all bandcamp urls to match to also to be more flexible
    if [[ "$LINK" =~ "/download/track" ]]; then
        downloadTrack
    else
        downloadAlbum
    fi
}

function downloadAlbum () {
    prettyLineBreak
    printf "Downloading...\n"
    prettyLineBreak
    FOLDER_NAME="bcsh-${uuid}" #Create tmp dir because we don't know the name yet.
    mkdir "./$FOLDER_NAME"
    cd "./$FOLDER_NAME"
    curl -O -J -L -- "$LINK" #if this download fails, script should stop due to set parameters
    ZIP_NAME=(*.zip)
    ZIP_NAME="${ZIP_NAME[0]}"
    cd ..
    mv "./$FOLDER_NAME" "./${ZIP_NAME%????} [WEB] [FLAC]"
    FOLDER_NAME="${ZIP_NAME%????} [WEB] [FLAC]"
    cd "./$FOLDER_NAME"
    unzip "./${ZIP_NAME}"
    rm "./${ZIP_NAME}"

    prettyLineBreak
}

function downloadTrack () {
    # Set variable to make sure we don't rename.
    isSingleTrack=1
    
    prettyLineBreak
    printf "Downloading Track...\n"
    prettyLineBreak
    
    #Create tmp dir because we don't know the name yet.
    FOLDER_NAME="bcsh-${uuid}"
    mkdir "$FOLDER_NAME"
    cd "$FOLDER_NAME"
    # Download single .flac hopefully.
    curl -O -J -L "$LINK"
    FLACNAME=(*.flac)
    FLACNAME="${FLACNAME[0]}"
    cd ..
    mv "./${FOLDER_NAME}" "./${FLACNAME%?????} [WEB] [Single] [FLAC]"
    FOLDER_NAME="${FLACNAME%?????} [WEB] [Single] [FLAC]"
    cd "./$FOLDER_NAME"
    prettyLineBreak
}

function addYearToFolder () {
    FLAC_LIST=(*.flac)

    # Add year to folder name
    if [[ "$METADATA_YEAR" == "AUTOMATIC" ]]; then
        METADATA_YEAR=$(getFlacTag DATE "${FLAC_LIST[0]}")
        if [ -z "$METADATA_YEAR" ]; then
            printf "[ERROR] Date Not found, Aborting.\n"
            return 2
        fi

        cd ..

        # Cleaner way to do this??? (w/ rename command?)
        if [ $isSingleTrack -eq 1 ]; then
            mv "./$FOLDER_NAME" "./${FOLDER_NAME: : ${#FOLDER_NAME}-22} [${METADATA_YEAR}] [Single] [WEB] [FLAC]"
            FOLDER_NAME="${FOLDER_NAME: : ${#FOLDER_NAME}-22} [${METADATA_YEAR}] [Single] [WEB] [FLAC]"
        else
            mv "./$FOLDER_NAME" "./${FOLDER_NAME: : ${#FOLDER_NAME}-13} [${METADATA_YEAR}] [WEB] [FLAC]"
            FOLDER_NAME="${FOLDER_NAME: : ${#FOLDER_NAME}-13} [${METADATA_YEAR}] [WEB] [FLAC]"
        fi
        cd "./$FOLDER_NAME"
    elif [[ $METADATA_YEAR != "NONE" ]]; then
        cd ..
        if [ $isSingleTrack -eq 1 ]; then
            mv "./${FOLDER_NAME}" "./${FOLDER_NAME: : ${#FOLDER_NAME}-22} [${METADATA_YEAR}] [Single] [WEB] [FLAC]"
            FOLDER_NAME="${FOLDER_NAME: : ${#FOLDER_NAME}-22} [${METADATA_YEAR}] [Single] [WEB] [FLAC]"
        else
            mv "./${FOLDER_NAME}" "./${FOLDER_NAME: : ${#FOLDER_NAME}-13} [${METADATA_YEAR}] [WEB] [FLAC]"
            FOLDER_NAME="${FOLDER_NAME: : ${#FOLDER_NAME}-13} [${METADATA_YEAR}] [WEB] [FLAC]"
            cd "./$FOLDER_NAME"
        fi
    fi
}

function renameTracks () {
    if [ $isSingleTrack -eq 0 ]; then
        FLAC_LIST=(*.flac)
        TRACK_COUNT="${#FLAC_LIST[@]}"

        for a in ./*.flac; do
            track_number=$(getFlacTag TRACKNUMBER "${a}")
            if [ -z "$track_number" ]; then
                printf "[ERROR] Track Number Not found, Aborting.\n"
                return 2
            fi
                
            track_title=$(getFlacTag TITLE "${a}")
            if [ -z "$track_title" ]; then
                printf "[ERROR] Track Title Not found, Aborting.\n"
                return 2
            fi

            while [ ${#TRACK_COUNT} -gt ${#track_number} ]; do
                track_number="0${track_number}"
            done
            
            # Replace invalid characters with - before moving.
            track_title=$(tr '\/:*?"<>|%' "-" <<< "$track_title")
            new_filename="${track_number} - ${track_title}.flac"

            # Truncate to 255 chars, if too long (generally file system limit is 255 characters)
            # Use wc because it apparently counts unicode characters correctly
            if (( $(wc -c <<< "${new_filename}") > 255 )); then
                # Aggressively reduce characters to hopefully avoid unicode hell when dealing with combining characters
                track_title=$(sed "s/[^[:alpha:].-]//g" <<< "$track_title")
                new_filename="${track_number} - ${track_title}"
                new_filename="${new_filename: : 160}.flac"
            fi

            mv "${a}" "./$new_filename"
        done
    else
        printf "Single Track, not renaming\n"
        prettyLineBreak
    fi
}

function checkAlbumTag () {
    for a in *.flac; do
        track_album=$(getFlacTag ALBUM "$a")
        if [ -z "$track_album" ]; then
            if [[ $isSingleTrack == 1 ]]; then
               # Set Album tag = Track Title
               printf "No Album Tag, Using Single Track's Title as Album\n"
               metaflac --no-utf8-convert --dont-use-padding --set-tag=ALBUM="$(getFlacTag TITLE "$a")" "$a"
               prettyLineBreak
            else
               printf "[WARNING] Album Name Not found, Check Tags!\n"
               PROBLEM_ALBUM_TAG_NOT_FOUND=1
               break
               prettyLineBreak
           fi
        fi
    done
}

function renameExtraFiles () {
    FLAC_LIST=(*.flac)
    track_album=$(getFlacTag ALBUM "${FLAC_LIST[0]}")
    track_album_artist=$(getFlacTag ALBUMARTIST "${FLAC_LIST[0]}")
    local FILE_PREFIX="${track_album_artist} - ${track_album}"
    FILE_PREFIX=$(tr '\/:*?"<>|' "-" <<< "$FILE_PREFIX")
    shopt -s nullglob # hopefully don't do anything if no matches
    set +u # Disable unbound variable checking in case there's nothing found
    if (( "${#ZIP_NAME}" >= 100)); then
        # Apparently Bandcamp truncates prefix to 100 characters
        FILE_PREFIX_TO_REMOVE="${FILE_PREFIX: : 100} - "
    else
        FILE_PREFIX_TO_REMOVE="${FILE_PREFIX} - "
    fi

    for file in *; do
        # ignore flac files, though they shouldn't really ever be a match regardless...
        if [[ "$file" == *.flac ]]; then
            continue
        fi

        if [[ "$file" =~ ^"${FILE_PREFIX_TO_REMOVE}" && "${#file}" -gt "${#FILE_PREFIX_TO_REMOVE}" ]]; then
            mv "./$file" "./${file: ${#FILE_PREFIX_TO_REMOVE}}"
        fi
    done

    set -u
    shopt -u nullglob # Return back to normal behavior (consider setting globally)
}

function transcode () {
    prettyLineBreak
    printf "Starting Transcode (better.py)\n"
    prettyLineBreak
    if [[  OTHERCOUNT -gt 0 ]]; then
        printf "[ERROR] Not Transcoding: Unknown bit-depths Found\n"
        return 0
    fi
    cd ..
    if [ $TORRENT_ANNOUNCE_URL == "0" ]; then
        better.py "${FOLDER_NAME}"
    else
        better.py -a "${TORRENT_ANNOUNCE_URL}" "${FOLDER_NAME}"
    fi
    cd "${FOLDER_NAME}"
}

function checkQuality () {
    printf "Checking Bitdepths // Sample Rate\n"
    prettyLineBreak

    COUNT16=0
    COUNT24=0
    OTHERCOUNT=0
    for a in *.flac; do
        file_metadata="$(metaflac --show-bps --show-sample-rate "$a")"
        bit_depth=$(sed -n 1p <<< "$file_metadata")
        sample_rate=$(sed -n 2p <<< "$file_metadata")

        if [ $bit_depth -eq "16" ]; then
            COUNT16=$((COUNT16+1))
            text_color='\033[1;32m'
        elif [ $bit_depth -eq "24" ]; then
            COUNT24=$((COUNT24+1))
            text_color='\033[1;36m'
        else
            OTHERCOUNT=$((OTHERCOUNT+1))
            text_color='\033[0;31m'
        fi
        printf "${text_color}${bit_depth}bit ${sample_rate}Hz: ${a}\033[0m\n"
    done
    prettyLineBreak
    CHECK_QUALITY_SUMMARY="\033[1;32m16Bit\033[0m: ${COUNT16} | \033[1;36m24Bit\033[0m: ${COUNT24} | \033[0;31mOther / Unknown\033[0m: ${OTHERCOUNT}"
    printf "${CHECK_QUALITY_SUMMARY}\n"

    if [[ "$isRunOnceCommand" == false ]]; then
        renameFolder
    else
        return 3
    fi
}

function renameFolder () {
    if [[ $COUNT24 -gt 0 && $COUNT16 -eq 0 && $OTHERCOUNT -eq 0 ]]; then
        cd ..
        mv "./$FOLDER_NAME" "./${FOLDER_NAME%?} 24bit]"
        FOLDER_NAME="${FOLDER_NAME%?} 24bit]"
        cd "./$FOLDER_NAME"
    elif [[ $COUNT24 -gt 0 && $COUNT16 -gt 0 && $OTHERCOUNT -eq 0 ]]; then
        cd ..
        mv "./$FOLDER_NAME" "./${FOLDER_NAME%?} Mixed]"
        FOLDER_NAME="${FOLDER_NAME%?} Mixed]"
        cd "./$FOLDER_NAME"
    elif [ $OTHERCOUNT -gt 0 ]; then
        printf "[ERROR] Unknown Bitdepths found... Aborting."
        return 3
    fi

}

function prettyLineBreak () {
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
}

function getFlacTag () {
    local flac_tag="$1"
    local filename="$2"
    local output=$(metaflac --show-tag="$flac_tag" "$filename")

    # Remove "TAG=" from output
    local tag_output_length=$(expr ${#flac_tag} + 1)
    printf "%s" "${output: ${tag_output_length}}"
}

function checkEmbededArt () {
    FLAC_LIST=(*.flac)
    copy_embeded_art=false

    # If there's two different kinds of files this might fail, but hopefully that should be rare...
    local MIMETYPE=$(metaflac --list --block-type PICTURE ${FLAC_LIST[0]} | grep MIME | tail -n1 | cut -b 14-)

    if [[ -z "$MIMETYPE" ]]; then
        return 0
    fi

    metaflac --export-picture-to="bcsh-cover-${uuid}" "${FLAC_LIST[0]}"
    for a in *.flac; do
        local PICTURE_SIZE=$(metaflac --list --block-type PICTURE  "$a" | grep "  length:" | cut -b 11- | sort -r | tail -n1)
        # Generally people don't like > 512KiB but check for 500~KiB for simplicity
        if (( "$PICTURE_SIZE" > 500000 )); then
            metaflac --no-utf8-convert --dont-use-padding --remove --block-type=PICTURE "$a"
            copy_embeded_art=true
        fi
    done

    # Copy Embedded artwork to cover.* if too large and cover.* doesn't already exist
    if [[ "$copy_embeded_art" == true && ! "$(find . -maxdepth 1 -name 'cover.*' -print -quit)" ]]; then
        echo "Copying Embedded Picture -> to local file"
        if [[ "$MIMETYPE" == "image/jpeg" ]]; then
            mv "bcsh-cover-${uuid}" "cover.jpg"
        elif [[ "$MIMETYPE" == "image/png" ]]; then
            mv "bcsh-cover-${uuid}" "cover.png"
        fi
    else
        rm "bcsh-cover-${uuid}"
    fi
}

function checkPathLengths () {
    PATH_TOO_LONG_FILES=()
    declare -i MAX_PATH_LENGTH=180
    
    for a in *; do
        local path_name="${PWD##*/}/${a}"
        if (( "${#path_name}"  > $MAX_PATH_LENGTH )); then
            PATH_TOO_LONG=true
            How_many_extra=$((${#path_name} - $MAX_PATH_LENGTH))
            main_dir="${PWD##*/}"
            if (( "${#main_dir}" >= $MAX_PATH_LENGTH )); then
                PATH_TOO_LONG_FILES+=("Folder Name > $MAX_PATH_LENGTH characters, consider renaming folder")
                break
            fi

            extension="${a##*.}"
            extoffset=$((${#extension} + 1))
            if [[ "$extension" == "$a" ]]; then
                good_part="${a:0: -$How_many_extra}"
                bad_part="${a:${#a} - $How_many_extra}"
                colorit="${good_part}\033[0;31m${bad_part}\033[0m"
            elif (( $((${How_many_extra} + ${extoffset})) < ${#a} )); then
                good_part="${a:0: -(($How_many_extra + $extoffset))}"
                bad_part="${a:((${#a} - $How_many_extra - $extoffset))}"
                bad_part_noext="${bad_part:0:((${#bad_part} - $extoffset))}"
                colorit="${good_part}\033[0;31m${bad_part_noext}\033[0m.${extension}"
            else
                PATH_TOO_LONG_FILES+=("(Path Length Too Long, consider renaming folder): ${a}")
                continue
            fi
            PATH_TOO_LONG_FILES+=("($(($How_many_extra + $MAX_PATH_LENGTH)) > $MAX_PATH_LENGTH): $colorit")
        fi
    done

    if [[ "$PATH_TOO_LONG" == true ]]; then
        printf "Paths > 180 Chars\n"
        for i in "${PATH_TOO_LONG_FILES[@]}"
        do
           printf "\t* ${i}\n"
        done
    fi
}

function makeTorrent () {
    if [ "$TORRENT_ANNOUNCE_URL" == "0" ]; then
        printf "Not Creating Torrent, TORRENT_ANNOUNCE_URL not set\n"
    else
        # Consider more robust version checking in the future
        VERSION_MKTORRENT=$(mktorrent -h | head -n1 | awk '{print $2}')

        cd ..
        if [[ "$TORRENT_SOURCE_FLAG" == "0" || "$VERSION_MKTORRENT" != "1.1" ]]; then
            mktorrent -p -a "${TORRENT_ANNOUNCE_URL}" "${FOLDER_NAME}"
        else
            mktorrent -p -s "${TORRENT_SOURCE_FLAG}" -a "${TORRENT_ANNOUNCE_URL}" "${FOLDER_NAME}"
        fi

        cd "./${FOLDER_NAME}"
    fi
}

function generateSpec () {
    if [ "$WEB_URL" == "https://changeme.example.com/specs/" ]; then
        prettyLineBreak
        printf "[ERROR] Not Generating Spectrals, WEB_URL (and probably WEB_DIRECTORY) not set\nSee Lines: 24 & 27\n"
        prettyLineBreak
        return 0
    fi

    WEB_URL="${WEB_URL}/${uuid}"
    WEB_DIRECTORY="${WEB_DIRECTORY}/${uuid}"
    mkdir "${WEB_DIRECTORY}"
    WEB_SPEC="${WEB_DIRECTORY}/index.html"
    
    prettyLineBreak
    printf "Generating Spectrals\n"
    prettyLineBreak

    printf '<!doctype html><html lang="en" style="background-color:#222;color:white;"><head><meta charset="utf-8"><meta http-equiv="x-ua-compatible" content="ie=edge"><meta name="viewport" content="width=device-width, initial-scale=1"><title></title></head><body style="margin:0;">' > $WEB_SPEC
    printf "%s" "<h1>${PWD##*/}</h1>" >> $WEB_SPEC
    function genSpec() {
        a="$1"
        printf "${a} :: "
        sox "$a" -n spectrogram -t "$a" -o "${WEB_DIRECTORY}/${a}.png"
    }

    if hash xargs 2>/dev/null; then
        # Run spec gen in parallel if xargs found
        declare -i NUM_PROCS
        NUM_PROCS=$(getconf _NPROCESSORS_ONLN)
        export -f genSpec
        export WEB_URL
        export WEB_DIRECTORY
        export uuid
        find . -name '*.flac' | sed 's/.*/"&"/' | xargs -I '{}' -P "$NUM_PROCS" bash -c 'genSpec "$@"' _ {}
    else
        # run sequentially if xargs not found
        for a in *.flac; do
            genSpec "$a"
        done
    fi

    for a in *.flac; do
        printf "<img src=\"${a}.png\" />" >> $WEB_SPEC
    done

    printf '</body></html>' >> $WEB_SPEC
    printf "\nSpectrals Generated: ${WEB_URL}\n"
}

function checkForExtraFiles () {
    shopt -s extglob
    shopt -s nullglob
    EXTRA_FILES=$(echo !(*.jpg|*.png|*.flac))
}

function runSummary () {
    prettyLineBreak
    printf "Final Summary\n"
    prettyLineBreak

    if [[ ! -z "$EXTRA_FILES" || $PROBLEM_ALBUM_TAG_NOT_FOUND -eq 1 || "$copy_embeded_art" == true || "$PATH_TOO_LONG" == true ]]; then
        printf "Warnings:\n"
    fi

    if [[ "$PATH_TOO_LONG" == true ]]; then
        printf "\033[0;31mFile Paths Too Long!!! (>180 chars):\033[0m\n"
        for i in "${PATH_TOO_LONG_FILES[@]}"
        do
           printf "\t* ${i}\n"
        done
    fi

    if [ ! -z "$EXTRA_FILES" ]; then
        printf "\t* \033[33mExtra Files\033[0m: ${EXTRA_FILES}\n"
    fi

    if [[ "$copy_embeded_art" == true ]]; then
        printf "$PICTURE_SIZE > 500000, Removed embedded art\n"
    fi

    if [ $PROBLEM_ALBUM_TAG_NOT_FOUND -eq 1 ]; then
        printf "\t* \033[0;31mAlbum Tag Not Found, Check Tags!\033[0m\n"
    fi

    printf "General Info:\n"
    printf "\t* %s\n" "Folder Name: $FOLDER_NAME"
    printf "\t* Bit Depth: ${CHECK_QUALITY_SUMMARY}\n"
    printf "\t* Spectrals: ${WEB_URL}\n"
}

function convertSecToHuman () {
    ((h=${1}/3600))
    ((m=(${1}%3600)/60))
    ((s=${1}%60))
    if (( "$h" != 0 )); then
        printf "%02d:%02d:%02d\n" $h $m $s
    else
        printf "%02d:%02d\n" $m $s
    fi
}

function makeTracklist () {
    printf "[size=4][b]Tracklist[/b][/size]\n"
    total_seconds=0
    for a in *.flac; do
        track_seconds=$(metaflac --show-total-samples --show-sample-rate "$a" | tr '\n' ' ' | awk '{print $1/$2}')
        total_seconds=$(awk '{print $1 + $2}' <<< " $total_seconds $track_seconds ")

        # Round up for the sake of display
        track_seconds=$(awk '{printf "%.0f", $1}' <<< " $track_seconds")

        track_number=$(getFlacTag TRACKNUMBER "$a")
        track_title=$(getFlacTag TITLE "$a")

        if [[ "$isCompilation" == true ]]; then
            track_artist=$(getFlacTag ARTIST "$a")
            printf "%s\n" "[b]${track_number}.[/b] [artist]${track_artist}[/artist] - ${track_title} [i]($(convertSecToHuman $track_seconds))[/i]"
        else
            printf "%s\n" "[b]${track_number}.[/b] ${track_title} [i]($(convertSecToHuman $track_seconds))[/i]"
        fi
    done

    total_seconds=$(awk '{printf "%.0f", $1}' <<< " $total_seconds")
    
    printf "[b]Total length[/b]: $(convertSecToHuman $total_seconds)\n"
    return 2
}

cleanup() {
    # Remove bcsh-uuid dir if empty on exit (Usually after an invalid url or command)
    if [ -d "../bcsh-${uuid}" ]; then
        cd ..
        rmdir "bcsh-${uuid}"
    fi
}

trap cleanup ERR
trap cleanup EXIT

#init some stuff
isSingleTrack=0
PROBLEM_ALBUM_TAG_NOT_FOUND=0
copy_embeded_art=false
PATH_TOO_LONG=false
isCompilation=false
isRunOnceCommand=false


if [ -z $1 ]; then
    showHelp
fi

set -u # Set unbound checking now that we've saved arguments (or lack thereof)

for i in "$@"; do
case $i in
    -h|--help)
    showHelp
    shift
    ;;
    --compilation)
    isCompilation=true
    shift
    ;;
    -y=*|--year=*)
    METADATA_YEAR="${i#*=}"
    shift
    ;;
    -y|--year)
    METADATA_YEAR="AUTOMATIC"
    shift
    ;;
    -s=*|--source=*)
    TORRENT_SOURCE_FLAG="${i#*=}"
    shift
    ;;
    -a=*|--announce=*)
    TORRENT_ANNOUNCE_URL="${i#*=}"
    shift
    ;;
    --tracklist)
    makeTracklist
    exit
    ;;
    --gen-spec)
    generateSpec
    exit
    ;;
    --check-quality)
    isRunOnceCommand=true
    checkQuality
    exit
    ;;
    --rename-tracks)
    read -p "Are you sure you want to rename all tracks in $(pwd)? This is irreversible: " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        renameTracks
        printf "Done\n"
    fi
    exit
    ;;
    --check-path-length)
    checkPathLengths
    exit
    ;;
    *)
    LINK="${i}"
    ;;
esac
done
main # Actually run stuff now that functions are loaded....
